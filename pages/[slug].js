import { Row, Col } from "react-bootstrap";
import Layout from "components/layout";
import HiglightCode from "components/higlight-code";
const BlockContent = require("@sanity/block-content-to-react");
import { getPostBySlug, getPaginatedPosts } from "lib/api";
import { urlFor } from "lib/api";
import moment from "moment";
import PostHeader from "components/post-header";
import { useRouter } from "next/router";
import PreviewAlert from "components/preview-alert";

const PostDetail = ({ post, preview }) => {
  moment.locale("mn");
  const router = useRouter();

  if (router.isFallback) {
    return (
      <Layout>
        <div>Loading...</div>
      </Layout>
    );
  }

  if (!router.isFallback && !post?.slug) {
    return (
      <Layout>
        <div>Уучлаарай ийм пост байхгүй байна.</div>
      </Layout>
    );
  }

  return (
    <Layout>
      <Row>
        <Col md="12">
          {/* <pre>{JSON.stringify(post, null, 2)}</pre> */}
          {preview && <PreviewAlert />}
          <PostHeader post={post} />
          <br />
          <BlockContent
            imageOptions={{ w: 320, h: 240, fit: "max" }}
            blocks={post.content}
            serializers={serializers}
          />
        </Col>
      </Row>
    </Layout>
  );
};

const serializers = {
  types: {
    code: (props) => (
      <HiglightCode language={props.node.language}>
        <code>{props.node.code}</code>
        <div className="code-filename">{props.node.filename}</div>
      </HiglightCode>
    ),
    image: (props) => (
      <div className={`blog-image blog-image-${props.node.position}`}>
        <img src={urlFor(props.node).height(300).url()} />
        <div className="code-filename">{props.node.alt}</div>
      </div>
    ),
  },
};

export const getStaticProps = async ({ params, preview = false }) => {
  const post = await getPostBySlug(params.slug, preview);
  return {
    props: {
      post: preview ? post[1] : post.length > 0 ? post[0] : {},
      preview,
    },
    revalidate: 6000,
  };
};

export const getStaticPaths = async () => {
  const posts = await getPaginatedPosts(0, 4);
  return {
    paths: posts.map((post) => ({
      params: {
        slug: post.slug,
      },
    })),
    fallback: true,
  };
};
export default PostDetail;
