import { Alert } from "react-bootstrap";
const PreviewAlert = () => {
  return (
    <Alert variant="danger">
      Анхаар та PREVIEW горимд байна!
      <Alert.Link href="/api/preview-exit">Энд дарж </Alert.Link> энэ горимоос
      гарна уу.
    </Alert>
  );
};
export default PreviewAlert;
