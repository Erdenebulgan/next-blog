const sanityClient = require("@sanity/client");

const client = sanityClient({
  projectId: process.env.NEXT_PUBLIC_SANITY_PROJECT_ID,
  dataset: process.env.NEXT_PUBLIC_SANITY_DATASET,
  useCdn: process.NODE_ENV === "production",
});

export const previewClient = sanityClient({
  projectId: process.env.NEXT_PUBLIC_SANITY_PROJECT_ID,
  dataset: process.env.NEXT_PUBLIC_SANITY_DATASET,
  useCdn: false,
  token:
    "skNQZZIYlVSBPkCafa51Zl9pxNSgPFfPIp1HuU1OecLg0sObLOTAMDADFNn9duBgl0T1ViM4ibpGe0El5jS3FfxEb77tDxL7BrWmZjEzyfAs9MjBuF1kcDM58cVVVws9pXpXBNMi8lqIJMqprvfRZTt4RAAqNZO2XUiAhwWiHDwJrqU8Ilsd",
});

export default client;
